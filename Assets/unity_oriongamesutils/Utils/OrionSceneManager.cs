﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

/// <summary>
/// Keep track of loaded scene for navigation purpose
/// </summary>
public class OrionSceneManager : Singleton<OrionSceneManager>
{

    private static Stack<string> _previousScenes = new Stack<string>();

    /// <summary>
    /// To keep track of scene name everywhere in the code
    /// </summary>
    public enum PossibleScenes
    {
        Options,
        Menu,
        Game,
        Lobby,
        Credit
    };

    public static void Back()
    {
        if (_previousScenes.Count == 0)
        {
            Debug.LogWarning("[OrionSceneManager] No more scene to back");
        }
        else
        {
            LoadLevelWithoutStack(_previousScenes.Pop());
        }
    }

    public static void LoadLevel(PossibleScenes level)
    {
        _previousScenes.Push(SceneManager.GetActiveScene().name);
        LoadLevelWithoutStack(level);
    }

    public static void LoadLevelWithoutStack(PossibleScenes level)
    {
        LoadLevelWithoutStack(level.ToString());
    }

    private static void LoadLevelWithoutStack(string level)
    {
        SceneManager.LoadScene(level);
    }
}
