﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class Asteroids : NetworkBehaviour {

    [SerializeField]
    private Sprite[] _spriteList;
    [SerializeField]
    private GameObject[] _spriteListExplosion;

    [SerializeField]
    private SpriteRenderer _spriteRenderer;


    [SerializeField]
    private CollisionDamage _coldamage;

	// Use this for initialization
	void Start () {
        int selected = Random.Range(0, _spriteList.Length);
        _spriteRenderer.sprite = _spriteList[selected];
        GetComponentInChildren<Destructible>()._prefabExplosion = _spriteListExplosion[selected];

        GetComponent<MovementOrder>()._speed = Random.value + 0.4f;

        if (isServer)
        {
            // Set Position
            Vector3 targetPos = new Vector3(Random.Range(-MapMouseEdge._gameBounds.extents.x, MapMouseEdge._gameBounds.extents.x),
                Random.Range(-MapMouseEdge._gameBounds.extents.y, MapMouseEdge._gameBounds.extents.y),
                0);

            Vector3 targetDir = targetPos - transform.position;
            targetDir.Normalize();

            GetComponent<MovementOrder>().SetTargetDirection(targetDir);

            // Set Power !
            float size = Random.Range(1, 5);
            transform.localScale = Vector3.one * (size * 0.7f);
            _coldamage._damage *= (int)size;

            // Set rotation somewhere else
            float space = Random.Range(0, 6);
            _spriteRenderer.transform.localPosition = Random.onUnitSphere * space;
            GetComponent<DistanceJoint2D>().distance = space;
	    }
    }
}
