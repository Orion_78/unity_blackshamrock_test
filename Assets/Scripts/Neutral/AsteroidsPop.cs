﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class AsteroidsPop : NetworkBehaviour{

    float timeToPopNext;

    [SerializeField]
    private PoolManagerNetwork _poolAsteroids;

    float GenerateTimeToPop()
    {
        return Random.Range(10, 30);
    }

    [ServerCallback]
    void Start()
    {
       
        timeToPopNext = GenerateTimeToPop();

        for (int i = 0; i < 5; i++)
        {
            PopAsteroid();
        }
    }




    // Update is called once per frame
    [ServerCallback]
    void Update()
    {
        timeToPopNext -= Time.deltaTime;

        if (timeToPopNext <= 0)
        {
            timeToPopNext = GenerateTimeToPop();

            PopAsteroid();
        }
	}

    [Server]
    void PopAsteroid()
    {
        Vector3 spawnPoint=  MapMouseEdge._realBounds.ClosestPoint(Random.onUnitSphere * float.MaxValue);
        Vector3 dir = -spawnPoint.normalized;

        spawnPoint -= dir * 5;
        Vector3 positionToPop = spawnPoint;
        positionToPop.z = 0;

        _poolAsteroids.Pop(positionToPop, 3);
    }
}
