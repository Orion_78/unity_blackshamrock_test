﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;

public class CollisionDamage : NetworkBehaviour {

    [SerializeField]
    public int _damage = 4;
    [SerializeField]
    public int _impactForce = 4;

    [SerializeField]
    public int _blastArea = -1;

    [SerializeField]
    private Destructible _destructible;

    [SerializeField]
    public GameObject _prefabDamage;

    [SerializeField]
    private Unit_ID _unitID;

    void Start()
    {
        if (_unitID == null)
        {
            _unitID = GetComponent<Unit_ID>();
        }

        if (_destructible == null)
        {
            _destructible = GetComponent<Destructible>();
        }
    }

    [ServerCallback]
    void OnCollisionEnter2D(Collision2D coll)
    {
        OnTriggerEnter2D(coll.collider);
    }

    [ServerCallback]
    void OnTriggerEnter2D(Collider2D other)
    {
        CollisionDamage cd = other.gameObject.GetComponent<CollisionDamage>();
        
        List<GameObject> targets = new List<GameObject>();
        if (cd == null && _blastArea > 0)
        {
            RaycastHit2D[] hits = Physics2D.CircleCastAll(transform.position, _blastArea, Vector3.forward);
            foreach(var h in hits)
            {
                targets.Add(h.collider.gameObject);
            }
        }
        else
        {
            targets.Add(other.gameObject);
        }

        bool atLeastOne = false;
        foreach (var t in targets)
        {
            Unit_ID id = t.GetComponent<Unit_ID>();
            

            // Dont do it with same camp
            if (id == null || id.GetPlayerIndex() == _unitID.GetPlayerIndex())
            {
                break;
            }

            Destructible d = t.GetComponent<Destructible>();
            

            if (d == null)
            {
                break;
            }

            atLeastOne = true;

            // Collision point
            RaycastHit hit;
            Vector3 dir = (t.transform.position - transform.position).normalized;
            if (Physics.Raycast(transform.position, dir, out hit))
            {
                RpcExplosion(hit.point);
                Explosion(hit.point);
            }

            Rigidbody2D r2d = t.GetComponent<Rigidbody2D>();
            r2d.AddForce(dir * _impactForce, ForceMode2D.Impulse);

            d.TakeDamage(_damage, _destructible);
        }

        if (atLeastOne && cd == null && _destructible != null)
        {
            _destructible.GoDead(_destructible);
        }
    }

    void Explosion(Vector3 pos)
    {
        if (_prefabDamage != null)
        {
            Instantiate(_prefabDamage, pos, Quaternion.identity);
        }
    }

    [ClientRpc]
    void RpcExplosion(Vector3 pos)
    {
        Explosion(pos);
    }
}
