﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class MinesPop : NetworkBehaviour
{

    [SerializeField]
    private PoolManagerNetwork _pool;

    [SerializeField]
    private int _numberToPop = 20;

    [SerializeField]
    private int _sideToPop = 0;

    [ServerCallback]
    void Start()
    {
        for (int i = 0; i < _numberToPop; i++)
        {
            Pop();
        }
    }


    [Server]
    void Pop()
    {
        Vector3 spawnPoint;

        bool RightLocation = true;
        int atemp = 10;
        do
        {
            spawnPoint = new Vector3(((Random.value * 2) - 1) * GetComponent<CircleCollider2D>().radius, ((Random.value * 2) - 1) * GetComponent<CircleCollider2D>().radius, 0);
            spawnPoint.z = 0;
            atemp--;

            RightLocation = true;
            RaycastHit2D[] hits = Physics2D.CircleCastAll(spawnPoint, 15, Vector3.forward);
            foreach (var h in hits)
            {
                if (h.collider.GetComponent<Destructible>() != null)
                {
                    RightLocation = false;
                    break;
                }
            }
        } while (!RightLocation && atemp > 0);
        _pool.Pop(spawnPoint, _sideToPop);
    }
}
