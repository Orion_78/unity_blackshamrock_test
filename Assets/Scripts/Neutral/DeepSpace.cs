﻿using UnityEngine;
using UnityEngine.Networking;

using System.Collections;

public class DeepSpace : NetworkBehaviour{

    [SerializeField]
    private Vector3 ColliderNormal;

	[ServerCallback]
    void OnCollisionEnter2D(Collision2D coll)
    {
        OnTriggerEnter2D(coll.collider);
    }

    [ServerCallback]
    void OnTriggerEnter2D(Collider2D other)
    {
        MovementOrder o = other.GetComponent<MovementOrder>();
        if (o != null)
        {
            o.BumpOnWall(ColliderNormal);
        }
        Rigidbody2D r = other.GetComponent<Rigidbody2D>();
            if (r != null)
            {
                r.velocity = Vector3.Reflect(r.velocity, ColliderNormal);
            }
        
    }
}
