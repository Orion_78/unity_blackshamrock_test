﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class AutoExplosion : NetworkBehaviour
{


    [SerializeField]
    private int _lifeTime = 5;

    private float _lifeTimeElapsed;


    private Destructible _destructible;



    private Unit_ID _unitID;


    void Start()
    {
        _unitID = GetComponent<Unit_ID>();

        _destructible = GetComponent<Destructible>();

            _destructible.HandleAlive += OnAlive;
    }

    [ServerCallback]
    void OnAlive(GameObject alive)
    {
        _lifeTimeElapsed = 0;
    }


    [ServerCallback]
    void Update()
    {
        _lifeTimeElapsed += Time.deltaTime;
        if (_lifeTimeElapsed > _lifeTime)
        {
            _destructible.GoDead(_destructible);
        }
    }
}
