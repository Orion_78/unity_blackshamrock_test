﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class BlackHole : NetworkBehaviour {

    [SerializeField]
    float maxeffect;

    [SerializeField]
    float radius;

    [SerializeField]
    Transform hole;


	// Update is called once per frame
    [ServerCallback]
	void Update () {

        foreach (var h in Physics2D.CircleCastAll(hole.position, radius, Vector3.forward))
        {
            Vector3 dir = hole.position - h.transform.position;
            float force = maxeffect * Time.deltaTime;
            dir.Normalize();
            Rigidbody2D r = h.collider.GetComponent<Rigidbody2D>();
            if (r != null)
            {
                r.AddForce(dir * force, ForceMode2D.Impulse);
            }

            MovementOrder mr = h.collider.GetComponent<MovementOrder>();
            if (mr != null)
            {
                mr.transform.rotation = Quaternion.Lerp(mr.transform.rotation, Quaternion.LookRotation(-dir, transform.forward), force);
           //     mr.transform.position = Vector3.Lerp(mr.transform.position, transform.position, force * 2);
            }
        }


	}
}
