﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class MovementOrder : NetworkBehaviour
{
    [SerializeField]
    public float _speed = 5;

    private Transform _target;
    private Vector3 _targetDirection;
    private bool _targetDirectionAffected;

    private Destructible _destructible;

    void Start()
    {

        _destructible = GetComponentInChildren<Destructible>();
        _destructible.HandleAlive += OnAlive;
    }

    [ServerCallback]
    void OnAlive(GameObject alive)
    {
        _target = null;
    }

    [Server]
    public void SetTarget(Transform newTarget)
    {
        _targetDirectionAffected = false;
        _target = newTarget;
    }

    [Server]
    public void SetTarget(Vector2 newTarget)
    {
        _targetDirectionAffected = false;
        transform.rotation = Util2D.LookAt2D(transform.position, newTarget);
    }

    [Server]
    public void SetTargetDirection(Vector2 direction)
    {
        _targetDirectionAffected = true;
        _targetDirection = direction;
    }



    [Server]
    public void BumpOnWall(Vector3 bounceNormalDirection)
    {
        if (_targetDirectionAffected)
        {
            _targetDirection = Vector3.Reflect(_targetDirection, bounceNormalDirection);
        }
    }

    [ServerCallback]
    void Update()
    {
        if (_target != null && _target.gameObject.activeSelf)
        {
            transform.rotation = Util2D.LookAt2D(transform.position, _target.transform.position);
        }

        if (_targetDirectionAffected)
        {
            transform.position += _targetDirection * _speed * Time.deltaTime;
        }
        else
        {
            transform.position += transform.up * _speed * Time.deltaTime;
        }
    }
}
