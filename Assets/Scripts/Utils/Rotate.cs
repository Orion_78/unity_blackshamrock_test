﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class Rotate : NetworkBehaviour{

    [SerializeField]
    private Transform toRotate;

    [SerializeField]
    private float speed = 5;

    Rigidbody2D rigid;

    void Start()
    {
        if (toRotate == null)
        {
            toRotate = GetComponent<Transform>();
        }
        
        rigid = GetComponent<Rigidbody2D>();
    }

	// Update is called once per frame
    [ServerCallback]
    void Update()
    {
       /* if (rigid != null)
        {
            rigid.AddTorque(speed * Time.deltaTime);
        }
        else
        {*/
        toRotate.Rotate(transform.forward, speed * Time.deltaTime);
        //}
    }
}
