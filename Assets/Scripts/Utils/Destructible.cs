#define DIE_ACTIVATION
//#define DIE_MOVE

//#define FADEINOUT

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.Networking;

public class Destructible : NetworkBehaviour
{
    [SyncVar]
    int life;
    [SerializeField]
    [SyncVar]
    int maxLife;

    [SerializeField]
    public GameObject _prefabExplosion;
    [SerializeField]
    public AudioClip[] _clipExplosion;
    [SerializeField]
    private GameObject _gameObjectToDestoy;
    
    
    [SerializeField]
    private Transform _transformExplosion;



    public Action<GameObject, Destructible> HandleDestroyed = delegate { };
    public Action<GameObject> HandleAlive = delegate { };

    public int GetLife()
    {
        return life;
    }
    public int GetMaxLife()
    {
        return maxLife;
    }

    void Start()
    {
        if (_gameObjectToDestoy == null)
        {
            _gameObjectToDestoy = gameObject;
        }

        if (_transformExplosion == null)
        {
            _transformExplosion = transform;
        }

        if (isServer)
        {
            life = maxLife;
        }
    }

    private Destructible _lastAttaking;

    float TimeInvulnerable = 2;

    [ServerCallback]
    void LateUpdate()
    {
        if (TimeInvulnerable > 0)
            TimeInvulnerable -= Time.deltaTime;
        if (life <= 0)
        {
            _gameObjectToDestoy.SetActive(false);
            GoDead(_lastAttaking);
        }
    }

    [Server]
    public void TakeDamage(int damage, Destructible attaking)
    {
        if (TimeInvulnerable > 0)
            return;

        life -= damage;

        _lastAttaking = attaking;
    }

    [Server]
    public void GoDead(Destructible attaking)
    {
        if (TimeInvulnerable > 0)
            return;

        // Get ready for a second life !
        life = maxLife;
        GoDeadNoBroadcart();
        HandleDestroyed(gameObject, attaking);
    }

    [Server]
    public void GoDeadNoBroadcart()
    {
        if (TimeInvulnerable > 0)
            return;

      //  RpcGoDeath();
        GoDeathExplosionEffect();
        RpcGoDeath();
        Destroy(_gameObjectToDestoy);
        NetworkServer.UnSpawn(_gameObjectToDestoy);
        
     //   NetworkServer.UnSpawn(gameObject);
    }

    [Server]
    public void GoAlive()
    {
        life = maxLife;
      //  RpcGoAlive();
        HandleAlive(gameObject);    
    }

    /*  [ClientRpc]
      private void RpcGoAlive()
      {
          gameObject.SetActive(true);
      }*/

    void GoDeathExplosionEffect()
    {
        if (_clipExplosion != null && _clipExplosion.Length > 0)
        {
            AudioManager.Instance.Play2DSound(_clipExplosion[UnityEngine.Random.Range(0, _clipExplosion.Length)], transform.position);
            
        }
        GameObject g = Instantiate(_prefabExplosion, _transformExplosion.position, transform.rotation) as GameObject;
         g.transform.localScale = _transformExplosion.localScale;
    }

    [ClientRpc]
    private void RpcGoDeath()
    {
       // gameObject.SetActive(false);
        GoDeathExplosionEffect();
     //   transform.position = UnityEngine.Random.onUnitSphere * 1000;
    }
   
}
