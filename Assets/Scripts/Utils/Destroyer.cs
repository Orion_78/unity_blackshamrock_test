﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class Destroyer : NetworkBehaviour {

	[ServerCallback]
    void OnCollisionEnter2D(Collision2D coll)
    {
        OnTriggerEnter2D(coll.collider);
    }

    [ServerCallback]
    void OnTriggerEnter2D(Collider2D other)
    {
        Destructible d = other.GetComponent<Destructible>();
        if (d != null)
        {
            d.GoDead(null);
        }
        else
        {
            Debug.LogError("You should not pass!");
            Destroy(other.gameObject);

        }
    }
}