﻿using UnityEngine;
using System.Collections;

public class Util2D : MonoBehaviour {

	public static Quaternion LookAt2D(Vector3 from, Vector3 to)
    {
        Vector3 diff = to - from;
        diff.Normalize();

        float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
        return Quaternion.Euler(0f, 0f, rot_z - 90);
    }
}
