﻿using UnityEngine;
using System.Collections;

public class ColliderFollower : MonoBehaviour {

    [SerializeField]
    private Transform tofollow;
    [SerializeField]
    private Collider2D _collider;

	// Update is called once per frame
	void LateUpdate () {
        _collider.offset = tofollow.localPosition;
	}
}
