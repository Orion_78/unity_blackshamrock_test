﻿using UnityEngine;
using System.Collections;

public class PoolStaticGetter : MonoBehaviour {

    public static PoolManagerNetwork PoolMissile;
    [SerializeField]
    private PoolManagerNetwork _poolMissile;

    void Awake()
    {
        PoolMissile = _poolMissile;
    }
}
