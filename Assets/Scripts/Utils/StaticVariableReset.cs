﻿using UnityEngine;
using System.Collections;

public class StaticVariableReset : MonoBehaviour {

	// Use this for initialization
	void Awake () {

        Player.totalID = 1;
        Player._myCurrentPlayerUniqueID = null;
        Player.hotSeat = false;
        Player.SpawningOrigin = null;
	}
}
