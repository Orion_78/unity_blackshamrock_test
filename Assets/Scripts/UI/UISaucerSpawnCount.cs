﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UISaucerSpawnCount : MonoBehaviour
{
    [SerializeField]
    private Slider _sliderAlien;
    [SerializeField]
    private Slider _sliderHuman;

    [SerializeField]
    private Text _textAlien;
    [SerializeField]
    private Text _textHuman;

    public void SliderAlien(float slider)
    {
        SpawnUnit.numberToSpawnUnitAlien = (int)slider;
    }

    public void SliderHuman(float slider)
    {
        SpawnUnit.numberToSpawnUnitHuman = (int)slider;
    }

    void Update()
    {
        _sliderAlien.value = SpawnUnit.numberToSpawnUnitAlien;
        _sliderHuman.value = SpawnUnit.numberToSpawnUnitHuman;

        _textAlien.text = SpawnUnit.numberToSpawnUnitAlien.ToString();

        _textHuman.text = SpawnUnit.numberToSpawnUnitHuman.ToString();
    }
}
