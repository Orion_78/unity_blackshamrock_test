﻿using UnityEngine;
using System.Collections;

public class OeilAnima : MonoBehaviour {

    Vector3 originePosition;

    Vector3 NextRandomPosition
    {
        get
        {
            return originePosition + Random.onUnitSphere * 10;
        }
    }

	// Use this for initialization
	void Start () {
        originePosition = transform.position;
        transform.position = NextRandomPosition;
        nextTime = 2 + Random.value * 7;
	}

    float nextTime;
	// Update is called once per frame
	void Update () {
        if (nextTime <= 0)
        {
            StopCoroutine("Next");
            StartCoroutine("Next");
            nextTime = 2 + Random.value * 7;
        }

        nextTime -= Time.deltaTime;
	}


    float TimeForNextAnimation;
    float startTimeForNextAnimation;
    IEnumerator Next()
    {
        Vector3 previousPos = transform.position;
        Vector3 nextPos = NextRandomPosition;
        TimeForNextAnimation = 0.5f + Random.value * 4;
        startTimeForNextAnimation = TimeForNextAnimation;

        while (TimeForNextAnimation > 0)
        {
            TimeForNextAnimation -= Time.deltaTime;

            transform.position = Vector3.Lerp(previousPos, nextPos, (startTimeForNextAnimation - TimeForNextAnimation) / startTimeForNextAnimation);

            yield return null;
        }

        transform.position = nextPos;
    }
}
