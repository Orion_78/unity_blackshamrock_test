﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UILifeBarShip : MonoBehaviour
{
    [SerializeField]
    private Slider _slider;
    [SerializeField]
    private Text _text;

    [SerializeField]
    private GamePlayMechanism.CurrentPlayerEnum _playerToFollow;

    
    void Update()
    {
        float numbToSpawn;
        if (_playerToFollow == GamePlayMechanism.CurrentPlayerEnum.PlayerAlien)
        {
            numbToSpawn = (float)SpawnUnit.numberToSpawnUnitAlien;
        }
        else
        {
            numbToSpawn = (float)SpawnUnit.numberToSpawnUnitHuman;
        }

        _slider.value = (float)Player.GetSaucerLeft(_playerToFollow) / numbToSpawn;
        _text.text = Player.GetSaucerLeft(_playerToFollow).ToString() + "/" + numbToSpawn.ToString();
    }
}
