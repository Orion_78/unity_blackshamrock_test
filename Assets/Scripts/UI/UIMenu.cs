﻿using UnityEngine;
using System.Collections;

public class UIMenu : MonoBehaviour {

    public void ButtonStart()
    {
        OrionSceneManager.LoadLevel(OrionSceneManager.PossibleScenes.Game);
    }

    public void ButtonMultiplayer()
    {
        OrionSceneManager.LoadLevel(OrionSceneManager.PossibleScenes.Lobby);
    }

    public void ButtonOptions()
    {
        OrionSceneManager.LoadLevel(OrionSceneManager.PossibleScenes.Options);
    }

    public void ButtonCredit()
    {
        OrionSceneManager.LoadLevel(OrionSceneManager.PossibleScenes.Credit);
    }
}
