﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UICurrentPlayer : MonoBehaviour
{
    [SerializeField]
    private Slider _slider;
    [SerializeField]
    private Text _sliderText;

    [SerializeField]
    private Image _image;
    [SerializeField]
    private Text _text;

    [SerializeField]
    private Sprite _imageAlien;
    [SerializeField]
    private Sprite _imageHuman;
  /*  [SerializeField]
    private Sprite _imageBreak;
    [SerializeField]
    private Sprite _imageWarmUp;*/

    [SerializeField]
    private GameObject _imageOnline;

    [SerializeField]
    private Animation _animation;

    // Use this for initialization
    IEnumerator Start()
    {
        yield return new WaitForSeconds(1);
        GamePlayMechanism.HandleCurrentPlayer += HandleCurrentPlayer;
        HandleCurrentPlayer(GamePlayMechanism.CurrentPlayer);
    }

    void HandleCurrentPlayer(GamePlayMechanism.CurrentPlayerEnum currentPlayer)
    {
        _text.text = "";
        _image.enabled = true;
        switch (currentPlayer)
        {
            case GamePlayMechanism.CurrentPlayerEnum.WarmUp:
                _image.enabled = false;
                _text.text = "Warm Up!";
                break;
            case GamePlayMechanism.CurrentPlayerEnum.PlayerAlien:
                _image.sprite = _imageAlien;
                break;
            case GamePlayMechanism.CurrentPlayerEnum.PlayerHuman:
                _image.sprite = _imageHuman;
                break;
            case GamePlayMechanism.CurrentPlayerEnum.Break:
                _image.enabled = false;
                _text.text = "Hold on!";
                break;
            default:
                break;
        }

        _animation.Play();
    }

    void Update()
    {
        _slider.value = GamePlayMechanism.CurrentTimeLeftToNextPhase / GamePlayMechanism.TimeToWaitFor;
        
        float val = GamePlayMechanism.CurrentTimeLeftToNextPhase * 10;
        val = Mathf.Floor(val);
        val /= 10;
        _sliderText.text = val.ToString();

        _imageOnline.SetActive(Player.IsItMyTurn);
    }
}
