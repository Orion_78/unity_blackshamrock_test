﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIDestructible : MonoBehaviour {

    [SerializeField]
    private Destructible _destructible;
    [SerializeField]
    private Slider _slider;

	// Update is called once per frame
	void Update () {
        _slider.value = (float)_destructible.GetLife() / (float)_destructible.GetMaxLife();
	}
}
