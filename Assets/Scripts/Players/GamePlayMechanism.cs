﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections;
using System;


public class GamePlayMechanism : NetworkBehaviour
{
    [SyncVar]
    private float _currentTimeLeftToNextPhase;
    [SyncVar]
    private float _timeToWaitFor;
    public static float CurrentTimeLeftToNextPhase;
    public static float TimeToWaitFor;

    [SyncVar(hook = "HandleCurrentPlayerAction")]
    private CurrentPlayerEnum _currentPlayer;
    public static CurrentPlayerEnum CurrentPlayer;
    public static Action<CurrentPlayerEnum> HandleCurrentPlayer;

    private float _timeWarmUp = 5;
    private float _timeOnePlayer = 10;
    private float _timeBreak = 3;

    [SerializeField]
    private GameObject _winCanvas;
    [SerializeField]
    private Text _winText;
    [SerializeField]
    private Button _buttonRemach;

    public enum CurrentPlayerEnum
    {
        WarmUp = -1,
        PlayerAlien = 2,
        PlayerHuman = 1,
        Break = 0
    }

    [SyncVar]
    private CurrentPlayerEnum _lastPlayerTurn;
    public static CurrentPlayerEnum LastPlayerTurn;

    void Awake()
    {
        GamePlayMechanism.HandleCurrentPlayer = delegate { };

        LastPlayerTurn = CurrentPlayerEnum.PlayerHuman;
        CurrentTimeLeftToNextPhase = _timeWarmUp;
        TimeToWaitFor = CurrentTimeLeftToNextPhase;
        CurrentPlayer = CurrentPlayerEnum.WarmUp;


        _lastPlayerTurn = LastPlayerTurn;
        _currentTimeLeftToNextPhase = CurrentTimeLeftToNextPhase;
        _timeToWaitFor = TimeToWaitFor;
        _currentPlayer = CurrentPlayer;
    }

    void HandleCurrentPlayerAction(CurrentPlayerEnum newPlayer)
    {
        CurrentPlayer = newPlayer;
        HandleCurrentPlayer(newPlayer);
    }

    float WaitTime = 10;
    bool gameIsOver = false;
    void Update()
    {
        if (isServer)
        {
            UpdateCurrentPhase();
        }

        CurrentTimeLeftToNextPhase = _currentTimeLeftToNextPhase;
        LastPlayerTurn = _lastPlayerTurn;
        TimeToWaitFor = _timeToWaitFor;

        if (WaitTime > 0 || gameIsOver)
        {
            WaitTime -= Time.deltaTime;
            return;
        }

        if (Player.GetSaucerLeft(CurrentPlayerEnum.PlayerAlien) == 0)
        {
            gameIsOver = true;
            _winCanvas.SetActive(true);

            if (Player.hotSeat)
            {
                _winText.text = "Human save the earth!\n\nThis is the independence day";
            }
            else
            {

                if (((CurrentPlayerEnum)Player.GetMyCurrentPlayerID()) == CurrentPlayerEnum.PlayerAlien)
                {
                    //Lose
                    _winText.text = "Human save the earth!\n\nBuild more shinny ship next time";
                    _buttonRemach.gameObject.SetActive(false);
                }
                else
                {
                    //Win
                    _winText.text = "Human save the earth!\n\nThis is the independence day";
                }
            }
        }
        else if (Player.GetSaucerLeft(CurrentPlayerEnum.PlayerHuman) == 0)
        {
            gameIsOver = true;
            _winCanvas.SetActive(true);

            if (Player.hotSeat)
            {
                _winText.text = "Aliens invade the earth!\n\nLet's bring shininess";
            }
            else
            {

                if (((CurrentPlayerEnum)Player.GetMyCurrentPlayerID()) == CurrentPlayerEnum.PlayerHuman)
                {
                    //Lose
                    _winText.text = "Aliens invade the earth!\n\nAll human race has been wiped out by shinny demons";
                    _buttonRemach.gameObject.SetActive(false);
                }
                else
                {
                    //Win
                    _winText.text = "Aliens invade the earth!\n\nLet's bring shininess";
                }
            }
        }
    }

    [Server]
    void UpdateCurrentPhase()
    {
        _currentTimeLeftToNextPhase -= Time.deltaTime;

        if (_currentTimeLeftToNextPhase <= 0)
        {
            switch (_currentPlayer)
            {
                case CurrentPlayerEnum.WarmUp:
                    _currentPlayer = CurrentPlayerEnum.PlayerAlien;
                    _currentTimeLeftToNextPhase = _timeOnePlayer;
                    break;
                case CurrentPlayerEnum.PlayerAlien:
                case CurrentPlayerEnum.PlayerHuman:
                    _lastPlayerTurn = _currentPlayer;
                    _currentPlayer = CurrentPlayerEnum.Break;
                    _currentTimeLeftToNextPhase = _timeBreak;
                    break;
                case CurrentPlayerEnum.Break:
                    if (_lastPlayerTurn == CurrentPlayerEnum.PlayerAlien)
                    {
                        _currentPlayer = CurrentPlayerEnum.PlayerHuman;
                    }
                    else
                    {
                        _currentPlayer = CurrentPlayerEnum.PlayerAlien;
                    }

                    _currentTimeLeftToNextPhase = _timeOnePlayer;
                    break;
                default:
                    break;
            }

            _timeToWaitFor = _currentTimeLeftToNextPhase;
        }
    }
}
