﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class OrderToFire : NetworkBehaviour {

	// Update is called once per frame
    [ClientCallback]
	void Update () {

        if (Input.GetButtonDown("Fire2"))
        {
            ClicToFire();
        }
       
	}

    void ClicToFire()
    {
        if (!Player.IsItMyTurn)
        {
            return;
        }

        for (int i = 0; i < SelectUnit.SelectedSaucers.Count; i++)
        {
            CmdUnitToFire(SelectUnit.SelectedSaucers[i].name, Camera.main.ScreenToWorldPoint(Input.mousePosition));
        }
    }

    [Command]
    // TODO Improve to one cmd (with max limit)
    private void CmdUnitToFire(string unit_ID, Vector3 targetPosition)
    {
        //foreach (var i in unit_ID)
        //{
        PoolManagerNetwork.AllObjectWithId[unit_ID].GetComponent<Canon>().Fire(targetPosition);
        //}
    }
}
