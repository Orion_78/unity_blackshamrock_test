﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine.UI;

public class SelectUnit : NetworkBehaviour
{
    public static List<Saucers> SelectedSaucers = new List<Saucers>();
    
    [SerializeField]
    RectTransform _rectTransform;

    Vector2 _mouseStart;
    Vector2 _mouseEnd;
    Vector3 _worldMouseStart;

    enum MouseStatus
    {
        NotOnScreen,
        Press,
        Drag,
    }

    MouseStatus _mouseStatus = MouseStatus.NotOnScreen;

    float _delayForClic = 0.5f;
    float _minDistanceToDrag = 10;
    float timeSinceLastClic;

    void Start()
    {
        SelectedSaucers = new List<Saucers>();
        SelectedSaucers.Clear();
        GamePlayMechanism.HandleCurrentPlayer += HandleCurrentPlayer;
    }

    void HandleCurrentPlayer(GamePlayMechanism.CurrentPlayerEnum player)
    {
        if (Player.hotSeat && player == GamePlayMechanism.CurrentPlayerEnum.Break)
        {
            CleanSelected();
        }
    }

    [ClientCallback]
    void Update()
    {
        if (CameraManage.CurrentViewStatus >= CameraManage.ViewStatus.Minimap)
        {
            HideSelectionCircle();
            CleanSelected();
            return;
        }

        // remove unselectable lux
        foreach (var l in SelectedSaucers)
        {
            if (l == null || l.gameObject == null || !l.gameObject.activeSelf)
            {
                if (l != null)
                {
                    l.IsSelected = false;
                }
                SelectedSaucers.Remove(l);
            }
        }

        UpdateClicSystem();
    }

    
    void UpdateClicSystem()
    {
        // First clic
        if (Input.GetButtonDown("Fire1"))
        {
            _mouseStatus = MouseStatus.Press;
            timeSinceLastClic = Time.timeSinceLevelLoad;

            // record origin mouse
            _mouseStart = Input.mousePosition;
            _worldMouseStart = Camera.main.ScreenToWorldPoint(_mouseStart);
        }
            // Drag
        else if (Input.GetButton("Fire1"))
        {
            _mouseEnd = Input.mousePosition;

            if (_mouseStatus == MouseStatus.Drag || Vector2.Distance(_mouseStart, _mouseEnd) > _minDistanceToDrag)
            {
                if (_mouseStatus != MouseStatus.Drag)
                {
                    CleanSelected();
                    _mouseStatus = MouseStatus.Drag;
                }

                DragToSelect();
            }
        }
            // Release
        else if (Input.GetButtonUp("Fire1"))
        {
            HideSelectionCircle();

            if (_mouseStatus == MouseStatus.Press && Time.timeSinceLevelLoad < timeSinceLastClic + _delayForClic)
            {
                ClicToMove();
            }

            _mouseStatus = MouseStatus.NotOnScreen;
        }
    }

    void HideSelectionCircle()
    {
        // Selection circle disapear
        _rectTransform.position = Vector2.one * -1;
        _rectTransform.sizeDelta = Vector2.zero;
    }

    /// <summary>
    /// Select rectangle and select unit
    /// </summary>
    void DragToSelect()
    {
        CleanSelected();

        Vector2 size = _mouseEnd - _mouseStart;

        Vector3 _worldMouseEnd = Camera.main.ScreenToWorldPoint(_mouseEnd);

        Vector3 worldSize = _worldMouseEnd - _worldMouseStart;

        Vector3 worldMiddle = _worldMouseStart + worldSize * 0.5f;
        worldSize = new Vector3(Mathf.Abs(worldSize.x), Mathf.Abs(worldSize.y), Mathf.Abs(worldSize.z));

        RaycastHit2D[] hits = Physics2D.BoxCastAll(worldMiddle, worldSize, 0, Vector3.forward);

        foreach (var h in hits)
        {
            Saucers l = h.collider.gameObject.GetComponent<Saucers>();

            if (l != null)
            {
                if (l.GetComponent<Unit_ID>().GetPlayerIndex() == Player.GetMyCurrentPlayerID())
                {
                    l.IsSelected = true;
                    SelectedSaucers.Add(l);
                }
            }
        }

        _rectTransform.position = new Vector2(size.x > 0 ? _mouseStart.x : _mouseEnd.x, size.y > 0 ? _mouseStart.y : _mouseEnd.y);
        _rectTransform.sizeDelta = new Vector2(Mathf.Abs(size.x), Mathf.Abs(size.y));
    }

    void ClicToMove()
    {
        if (!Player.IsItMyTurn)
        {
            return;
        }

        for (int i = 0; i < SelectedSaucers.Count; i++)
        {
            //idList[i] = _selectedLux[i].name;
            CmdUnit(SelectedSaucers[i].name, Camera.main.ScreenToWorldPoint(Input.mousePosition));
        }

       // CleanSelected();
    }

    [Command]
    // TODO Improve to one cmd (with max limit)
    private void CmdUnit(string unit_ID, Vector3 targetPosition)
    {
        //foreach (var i in unit_ID)
        //{
        PoolManagerNetwork.AllObjectWithId[unit_ID].GetComponent<Saucers>().ChangePositionOrder(targetPosition);
        //}
    }

    void CleanSelected()
    {
        foreach (var l in SelectedSaucers)
        {
            if (l != null)
            {
                l.IsSelected = false;
            }
        }
        SelectedSaucers.Clear();
    }
}
