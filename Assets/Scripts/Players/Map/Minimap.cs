﻿using UnityEngine;
using System.Collections;

public class Minimap : MonoBehaviour {
    
    Vector3 _normalPosition;

    public static float camFinalSize = 55;

    Vector3 ZoomCamPos = new Vector3(0, 0, -10);

    void Start()
    {
        camFinalSize = 55;
    }

	// Update is called once per frame
	void Update () {
        if (Input.GetButtonDown("Minimap"))
        {
            CameraManage.CurrentViewStatus = CameraManage.ViewStatus.Minimap;
            _normalPosition = Camera.main.transform.position;
            CameraManage.CamTargetPosition = ZoomCamPos;
            CameraManage.CamTargetSize = camFinalSize;
        }
        else if (Input.GetButtonUp("Minimap"))
        {
            CameraManage.CurrentViewStatus = CameraManage.ViewStatus.Free;
            CameraManage.CamTargetPosition = _normalPosition;
            CameraManage.CamTargetSize = CameraManage.CamSizeOrigin;
        }
	}
}
