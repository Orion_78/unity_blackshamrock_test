﻿using UnityEngine;
using System.Collections;

public class CameraManage : MonoBehaviour {

    public static ViewStatus CurrentViewStatus;

    public enum ViewStatus
    {
        Free = 0,
        MouseEdge = 1,
        NextUnit = 2,
        Minimap = 3,
        Blocked
    }

    public static Vector3 CamTargetPosition;

    public static float CamTargetSize;

    public static float CamSizeOrigin;

    [SerializeField]
    private float speed = 5;

    float speedOrig;

    IEnumerator Start()
    {
        CurrentViewStatus = ViewStatus.Free;
        CamTargetPosition = Vector3.zero;
        CamTargetSize = 0;
        CamSizeOrigin = 0;

        CamSizeOrigin = Camera.main.orthographicSize;
        
        CurrentViewStatus = ViewStatus.Blocked;

        Camera.main.orthographicSize = Minimap.camFinalSize;
        CamTargetSize = Camera.main.orthographicSize;
        Camera.main.transform.position = new Vector3(0, 0, -10) ;

        while (Player.SpawningOrigin == null)
        {
            yield return null;
        }

        CamTargetPosition = Player.SpawningOrigin.transform.position;

        speedOrig = speed;
        CamTargetSize = CamSizeOrigin;
        
        speed = 1.5f;
        Invoke("GoBackSpeed", 3);
    }

    void GoBackSpeed()
    {
        speed = speedOrig;
        CurrentViewStatus = ViewStatus.Free;
    }

    void LateUpdate()
    {
        CamTargetPosition.z = -10;
        CamTargetPosition = MapMouseEdge._gameBounds.ClosestPoint(CamTargetPosition);

        Camera.main.transform.position = Vector3.Lerp(Camera.main.transform.position, CamTargetPosition, Time.deltaTime * speed);
        Camera.main.orthographicSize = Mathf.Lerp(Camera.main.orthographicSize, CamTargetSize, Time.deltaTime * speed);
    }
}
