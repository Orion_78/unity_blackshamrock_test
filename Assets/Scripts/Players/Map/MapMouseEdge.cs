﻿using UnityEngine;
using System.Collections;

public class MapMouseEdge : MonoBehaviour
{
    public int Boundary = 50; // distance from edge scrolling starts
    public int speed = 5;

    public int screenBorderX = 50;
    public int screenBorderY = 50;

    private int theScreenWidth;
    private int theScreenHeight;

    [SerializeField]
    private SpriteRenderer _backgroundBounds;
    public static Bounds _gameBounds;
    public static Bounds _realBounds;

    void Start() 
    {
     
        theScreenWidth = Screen.width;
        theScreenHeight = Screen.height;

        _realBounds = _backgroundBounds.bounds;
        Vector3 boundsSize = _realBounds.size;
        boundsSize.x -= screenBorderX;
        boundsSize.y -= screenBorderY;
        boundsSize.z = 10;
        _gameBounds.size = boundsSize;
        
        // Todo think for 1/4 of height and 1/5 of width
    }

    void LateUpdate() 
    {
        Vector3 oldPosition = transform.position;
        if (CameraManage.CurrentViewStatus <= CameraManage.ViewStatus.MouseEdge)
        {

            CameraManage.CurrentViewStatus = CameraManage.ViewStatus.Free;

            Camera.main.rect = new Rect(Vector2.zero, Vector2.one * 3);//.size = Vector2.one * 2;

            if (Input.mousePosition.x > theScreenWidth - Boundary && Input.mousePosition.x <= theScreenWidth)
            {
                CameraManage.CurrentViewStatus = CameraManage.ViewStatus.MouseEdge;
                CameraManage.CamTargetPosition.x += speed * Time.deltaTime; // move on +X axis
            }
            if (Input.mousePosition.x < 0 + Boundary && Input.mousePosition.x >= 0)
            {

                CameraManage.CurrentViewStatus = CameraManage.ViewStatus.MouseEdge;
                CameraManage.CamTargetPosition.x -= speed * Time.deltaTime; // move on -X axis
            }
            if (Input.mousePosition.y > theScreenHeight - Boundary && Input.mousePosition.y <= theScreenHeight)
            {

                CameraManage.CurrentViewStatus = CameraManage.ViewStatus.MouseEdge;
                CameraManage.CamTargetPosition.y += speed * Time.deltaTime; // move on +Z axis
            }
            if (Input.mousePosition.y < 0 + Boundary && Input.mousePosition.y >= 0)
            {

                CameraManage.CurrentViewStatus = CameraManage.ViewStatus.MouseEdge;
                CameraManage.CamTargetPosition.y -= speed * Time.deltaTime; // move on -Z axis
            }
        }

    }
}
