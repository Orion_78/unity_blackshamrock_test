﻿using UnityEngine;
using System.Collections;

public class SelectNextUnit : MonoBehaviour {

    private int CurrentUnit;
    float timeToFocus = 0.5f;

    void Update()
    {
        if (CameraManage.CurrentViewStatus >= CameraManage.ViewStatus.Minimap)
        {
            return;
        }

        if (Input.GetButtonDown("Left") || Input.GetButtonDown("Right"))
        {
            
            GameObject[] mySaucers = Player.GetMyCurrentPlayerSaucer();

            if (Input.GetButtonDown("Right"))
            {
                CurrentUnit = (CurrentUnit + 1) % mySaucers.Length;
            }
            else if (Input.GetButtonDown("Left"))
            {
                CurrentUnit = CurrentUnit - 1;
                if (CurrentUnit < 0)
                {
                    CurrentUnit = mySaucers.Length - 1;
                }
                    
            }

            CameraManage.CamTargetPosition = mySaucers[CurrentUnit].transform.position;

           // StopCoroutine("FocusCamera");
          //  StartCoroutine("FocusCamera");
            Debug.Log("Goto " + CurrentUnit);
        }
    }
    /*
    Vector3 CamDest;
    IEnumerator FocusCamera()
    {
        CameraManage.CurrentViewStatus = CameraManage.ViewStatus.NextUnit;
        float t = 0;
        CamDest.z = -10;

        Vector3 oldPos = Camera.main.transform.position;

        while (t < timeToFocus)
        {
            t += Time.deltaTime;
            Camera.main.transform.position = Vector3.Lerp(oldPos, CamDest, t / timeToFocus);

            if (Minimap.CurrentViewStatus != Minimap.ViewStatus.NextUnit)
            {
                yield break;
            }

            yield return null;
        }
        Camera.main.transform.position = CamDest;
        Minimap.CurrentViewStatus = Minimap.ViewStatus.Free;
    }*/

}
