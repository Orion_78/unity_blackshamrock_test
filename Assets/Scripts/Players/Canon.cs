﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class Canon : NetworkBehaviour{

    private Unit_ID _unit_ID;

    [SerializeField]
    private float _reloadTime = 1;

    [SerializeField]
    private AudioClip _shootSound ;

    private float TimeLastFire;

    void Start()
    {
        _unit_ID = GetComponent<Unit_ID>();
    }

    [Server]
    public void Fire(Vector3 target)
    {
        // TODO improve for when reloading game
        if (TimeLastFire + _reloadTime < Time.timeSinceLevelLoad)
        {
            AudioManager.Instance.Play2DSound(_shootSound, transform.position);
            RpcFireSound();

            TimeLastFire = Time.timeSinceLevelLoad;

            // Fire
            GameObject o = PoolStaticGetter.PoolMissile.Pop(transform.position, _unit_ID.GetPlayerIndex());

            o.GetComponent<MovementOrder>().SetTarget(target);
        }
    }

    [ClientRpc]
    void RpcFireSound()
    {
        AudioManager.Instance.Play2DSound(_shootSound, transform.position);
    }
}
