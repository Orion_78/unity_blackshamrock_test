﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class SpawnUnit : NetworkBehaviour {

    public static int numberToSpawnUnitAlien = 7;
    public static int numberToSpawnUnitHuman = 7;

    [SerializeField]
    private PoolManagerNetwork _toSpawn;

    private Unit_ID _unitID;

	// Use this for initialization
	//[ServerCallback]
    IEnumerator Start () {

        _unitID = GetComponent<Unit_ID>();

        // Spawn units
        if (isServer)
        {
            int toSpawn;
            if (((GamePlayMechanism.CurrentPlayerEnum)_unitID.GetPlayerIndex()) == GamePlayMechanism.CurrentPlayerEnum.PlayerAlien)
            {
                toSpawn = numberToSpawnUnitAlien;
            }
            else
            {
                toSpawn = numberToSpawnUnitHuman;
            }

            for (int i = 0; i < toSpawn; i++)
            {
                GameObject o = _toSpawn.Pop(transform.position, _unitID.GetPlayerIndex());
                o.transform.parent = transform;

                o.GetComponent<Saucers>().ChangePositionOrder(transform.position + Random.onUnitSphere);

                o.transform.tag = ((GamePlayMechanism.CurrentPlayerEnum)_unitID.GetPlayerIndex()).ToString();
            }
        }
         while (Player.GetMyCurrentPlayerID() == -1)
        {
            yield return null;
        }

         if (Player.GetMyCurrentPlayerID() == _unitID.GetPlayerIndex())
         {
             Player.SpawningOrigin = gameObject;
         }
	}

    [ServerCallback]
    void Update()
    {
        RpcUpdateHumanAlien(SpawnUnit.numberToSpawnUnitHuman, SpawnUnit.numberToSpawnUnitAlien);
    }

            [ClientRpc]
    void RpcUpdateHumanAlien(int a, int b)
    {
        SpawnUnit.numberToSpawnUnitHuman = a;

        SpawnUnit.numberToSpawnUnitAlien = b;
    }
}
