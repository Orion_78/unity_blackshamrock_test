﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;

public class Player : NetworkBehaviour
{
    public static int totalID = 1;
    public static Unit_ID _myCurrentPlayerUniqueID;

    public static bool hotSeat;

    public static GameObject[] GetMyCurrentPlayerSaucer()
    {
        return GameObject.FindGameObjectsWithTag(((GamePlayMechanism.CurrentPlayerEnum)GetMyCurrentPlayerID()).ToString());
    }

    public static GameObject SpawningOrigin;
    

    public static int GetSaucerLeft(GamePlayMechanism.CurrentPlayerEnum playerSide)
    {
        return GameObject.FindGameObjectsWithTag(playerSide.ToString()).Length;
    }
    
    public static int GetMyCurrentPlayerID()
    {
        if (hotSeat)
        {
            if (GamePlayMechanism.LastPlayerTurn == GamePlayMechanism.CurrentPlayerEnum.PlayerHuman)
            {
                return (int)GamePlayMechanism.CurrentPlayerEnum.PlayerAlien;
            }
            else
            {
                return (int)GamePlayMechanism.CurrentPlayerEnum.PlayerHuman;
            }
        }
        else if (_myCurrentPlayerUniqueID == null)
        {
            return -1;
        }
        else
        {
            return _myCurrentPlayerUniqueID.GetPlayerIndex();
        }
    }

    public static bool IsItMyTurn
    {
        get
        {
            if (_myCurrentPlayerUniqueID == null)
            {
                return false;
            }
            return (int)GamePlayMechanism.CurrentPlayer == Player.GetMyCurrentPlayerID();
        }
    }

     void OnDestroy()
    {
        totalID--;
    }


    void Start()
    {
        

        if (isServer)
        {
            GetComponent<Unit_ID>().SetPlayerIndex(totalID);
            GetComponent<Unit_ID>().SetMyUniqueID("Player" + totalID);
            totalID++;
        }
        if (isLocalPlayer)
        {
            if (_myCurrentPlayerUniqueID != null)
                hotSeat = true;
            _myCurrentPlayerUniqueID = GetComponent<Unit_ID>();
        }
    }
        
}
