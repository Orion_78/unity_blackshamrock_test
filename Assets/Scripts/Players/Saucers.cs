﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System;


public class Saucers : NetworkBehaviour
{
    private Vector3 NextPosition;
  
    [SerializeField]
    private GameObject _selectedUI;

    private bool _isSelected;
    public bool IsSelected
    {
        get
        {
            return _isSelected;
        }
        set
        {
            _isSelected = value;
            _selectedUI.SetActive(IsSelected);
        }
    }

    private static long _speed = 1;

    Destructible _destructible;
    Unit_ID _unitID;
    void Start()
    {
        _destructible = GetComponent<Destructible>();
        _unitID = GetComponent<Unit_ID>();

        IsSelected = IsSelected;

        if (isServer)
        {
            _destructible.HandleDestroyed += HandleDeath;
        }
    }

    void HandleDeath(GameObject o, Destructible d)
    {
        _unitID.SetPlayerIndex(Unit_ID.NeutralSide);
    }

    [Server]
    public void ChangePositionOrder(Vector2 nextPosition)
    {
        NextPosition = nextPosition;
    }
    
    
    void Update()
    {
        if (isServer)
        {
            float deltaSpeed = _speed * Time.deltaTime;

            if (Vector3.Distance(transform.position, NextPosition) < deltaSpeed)
            {
                transform.position = Vector3.Lerp(transform.position, NextPosition, deltaSpeed);
            }
            else
            {
                Vector3 direction = NextPosition - transform.position;
                direction.Normalize();
                transform.position += direction * deltaSpeed;
            }

            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.identity, Time.deltaTime * _speed * 5);
        }

        if (isClient)
        {
            Color c = _selectedUI.GetComponent<SpriteRenderer>().color;
            c.a = Player.IsItMyTurn ? 1 : 0.5f;
            _selectedUI.GetComponent<SpriteRenderer>().color = c;
        }

    }
}
