﻿using UnityEngine;
using System.Collections;

public class SaucerSprite : MonoBehaviour {

    Unit_ID _unitID;
    [SerializeField]
    private Sprite _humanSprite;
    [SerializeField]
    private Sprite _alienSprite;

    private SpriteRenderer _sprite;

    void Start()
    {
        _sprite = transform.GetChild(0).GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        _unitID = GetComponent<Unit_ID>();
        if (_unitID.GetPlayerIndex() == 1)
        {
            _sprite.sprite = _humanSprite;
        }
        else
        {
            _sprite.sprite = _alienSprite;
        }

    }

    
}
