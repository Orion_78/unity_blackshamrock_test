﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class Unit_ID : NetworkBehaviour
{
    public const int NeutralSide = 0;

    [SyncVar]
    [SerializeField]
    private int PlayerIndex;

    [HideInInspector]
    [SyncVar]
    private string my_uniqueID;
    private Transform myTransform;

    public bool IsReady()
    {
        return my_uniqueID != "";
    }

    public int GetPlayerIndex()
    {
        return PlayerIndex;
    }

    public void SetPlayerIndex(int number)
    {
        /*if (number > 2)
        {
            Debug.LogError("Here !");
        }*/
        PlayerIndex = number;
    }

    public void SetMyUniqueID(string id)
    {
        my_uniqueID = id;
    }

    public string GetMyUniqueID()
    {
        return my_uniqueID;
    }

    // Use this for initialization
    void Start()
    {
        myTransform = transform;
    }

    // Update is called once per frame
    void Update()
    {
        // Set Identity
        myTransform.name = my_uniqueID;
    }

    void LateUpdate()
    {
      //  gameObject.layer = PlayerIndex + 10;
    }
}
