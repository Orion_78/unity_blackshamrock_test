﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Networking;
using System;

public class PoolManagerNetwork : NetworkBehaviour
{
    protected Stack<GameObject> pool = new Stack<GameObject>();
    public static Dictionary<string, GameObject> AllObjectWithId = new Dictionary<string,GameObject>();

    [HideInInspector]
    public Transform root;

    public GameObject objectToInstantiate;

    public int numberToPreInstantiate;

    private static int nextNameId = 0;

    void Awake()
    {
        AllObjectWithId = new Dictionary<string, GameObject>();
        nextNameId = 0;
    }

    protected virtual void Start()
    {
        
        

        root = transform;

        for (int j = 0; j < numberToPreInstantiate; j++)
        {
            GameObject o = Generate(Vector3.zero, 0);
            o.GetComponent<Destructible>().GoDeadNoBroadcart();

            pool.Push(o);

        }
    }

    protected virtual GameObject Generate(Vector3 position, int playerIndex)
    {
        GameObject toPop = Instantiate(objectToInstantiate) as GameObject;

        toPop.name = nextNameId.ToString();
        nextNameId++;

        toPop.transform.parent = root;

        if (toPop.transform.GetComponentInChildren<Destructible>() != null)
        {
            toPop.transform.GetComponentInChildren<Destructible>().HandleDestroyed += OnDeath;
        }
        toPop.GetComponent<Unit_ID>().SetMyUniqueID(toPop.name);
        toPop.GetComponent<Unit_ID>().SetPlayerIndex(playerIndex);

        UpdatePosition(toPop, position);
        AllObjectWithId.Add(toPop.name, toPop);

        NetworkServer.Spawn(toPop);

        return toPop;
    }

    [Server]
    public GameObject Pop(Vector3 position, int playerIndex)
    {
       
        GameObject toPop;

        try
        {
            toPop = pool.Pop();
            toPop.GetComponent<Unit_ID>().SetPlayerIndex(playerIndex);
            UpdatePosition(toPop, position);
        }
        catch (System.InvalidOperationException)
        {
            toPop = Generate(position, playerIndex);
        }

        if (toPop.GetComponentInChildren<Destructible>() != null)
        {
            toPop.GetComponentInChildren<Destructible>().GoAlive();
        }
        return toPop;
    }

    private void UpdatePosition(GameObject go, Vector3 position)
    {

        go.GetComponent<Unit_SyncPosition>().SetPosRot(position, Quaternion.identity);
    //    go.transform.position = position;
    }

    public void OnDeath(GameObject deadObject, Destructible attaking)
    {
        AllObjectWithId.Remove(deadObject.name);
      //  pool.Push(deadObject);
    }
}
