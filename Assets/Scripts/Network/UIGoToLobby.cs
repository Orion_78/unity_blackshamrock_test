﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class UIGoToLobby : MonoBehaviour {

    public void ButtonGoLobby()
    {
        GameObject.Find("NetworkManager").GetComponent<NetworkLobbyManager>().SendReturnToLobby();
    }
}
