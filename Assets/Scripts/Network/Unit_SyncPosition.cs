﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class Unit_SyncPosition : NetworkBehaviour
{

    [SyncVar]
    private Vector3 syncPos;
    [SyncVar]
    private float syncZRot;
    [SyncVar]
    private string _tag = "Untagged";

    private Vector3 lastPos;
    private Quaternion lastRot;
    private Transform myTransform;
    private float lerpRate = 10;
    private float posThreshold = 0;//0.5f;
    private float rotThreshold = 0;//5;

    // Use this for initialization
    void Start()
    {
        myTransform = transform;
      //  GetComponent<Destructible>().HandleAlive += OnAlive;
    }

    [ClientCallback]
    void OnEnable()
    {
        lastPos = syncPos;
        lastRot = Quaternion.Euler(0,0,syncZRot);
        transform.position = lastPos;
        transform.rotation = lastRot;
    }

    [Server]
    public void SetPosRot(Vector3 pos, Quaternion rot)
    {
        syncPos = pos;
        syncZRot = rot.eulerAngles.z;

        myTransform = transform;

        myTransform.position = pos;
        myTransform.rotation = rot;

        lastPos = pos;
        lastRot = rot;
    }

    // Update is called once per frame
    void Update()
    {
        TransmitMotion();
        LerpMotion();
    }

    [ServerCallback]
    void TransmitMotion()
    {
        _tag = transform.tag ;

        if (Vector3.Distance(myTransform.position, lastPos) > posThreshold || Quaternion.Angle(myTransform.rotation, lastRot) > rotThreshold)
        {
            lastPos = myTransform.position;
            lastRot = myTransform.rotation;

            syncPos = myTransform.position;
            syncZRot = myTransform.localEulerAngles.z;
        }
    }

    [ClientCallback]
    void LerpMotion()
    {
        myTransform.position = Vector3.Lerp(myTransform.position, syncPos, Time.deltaTime * lerpRate);

        Vector3 newRot = new Vector3(0, 0, syncZRot);
        myTransform.rotation = Quaternion.Lerp(myTransform.rotation, Quaternion.Euler(newRot), Time.deltaTime * lerpRate);

        transform.tag = _tag;
    }


}
